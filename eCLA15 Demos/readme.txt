The subfolder "music" which contains the music snippets
is not included in the Swish Bitbucket repository.
The music files are only required for the media_player
demo to correctly play audio when an album cover is
selected. It is not required for Swish.