﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="eCLA15 Demos" Type="Folder">
			<Item Name="subvis" Type="Folder">
				<Item Name="_mp_album_cover_tap.vi" Type="VI" URL="../eCLA15 Demos/subvis/_mp_album_cover_tap.vi"/>
				<Item Name="_mp_play_wav_sample.vi" Type="VI" URL="../eCLA15 Demos/subvis/_mp_play_wav_sample.vi"/>
			</Item>
			<Item Name="typedefs" Type="Folder">
				<Item Name="album_image_pic.ctl" Type="VI" URL="../eCLA15 Demos/typedefs/album_image_pic.ctl"/>
				<Item Name="leftside_options.ctl" Type="VI" URL="../eCLA15 Demos/typedefs/leftside_options.ctl"/>
				<Item Name="slider_white.ctl" Type="VI" URL="../eCLA15 Demos/typedefs/slider_white.ctl"/>
				<Item Name="volume_global.vi" Type="VI" URL="../eCLA15 Demos/typedefs/volume_global.vi"/>
			</Item>
			<Item Name="media_player.vi" Type="VI" URL="../eCLA15 Demos/media_player.vi"/>
			<Item Name="media_player_XGA.vi" Type="VI" URL="../eCLA15 Demos/media_player_XGA.vi"/>
		</Item>
		<Item Name="LabVIEW&gt;Project" Type="Folder">
			<Item Name="typedefs" Type="Folder">
				<Item Name="_SC_state.ctl" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/typdefs/_SC_state.ctl"/>
				<Item Name="_SC_state_nested_index.ctl" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/typdefs/_SC_state_nested_index.ctl"/>
				<Item Name="_SC_state_data.ctl" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/typdefs/_SC_state_data.ctl"/>
				<Item Name="_SC_swish_def_graphic.ctl" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/typdefs/_SC_swish_def_graphic.ctl"/>
				<Item Name="_SC_UE_cluster.ctl" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/typdefs/_SC_UE_cluster.ctl"/>
				<Item Name="_SC_UE_subj_enum.ctl" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/typdefs/_SC_UE_subj_enum.ctl"/>
				<Item Name="_SC_UE_ref.ctl" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/typdefs/_SC_UE_ref.ctl"/>
				<Item Name="_SC_UE_registration_ref.ctl" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/typdefs/_SC_UE_registration_ref.ctl"/>
			</Item>
			<Item Name="subvis" Type="Folder">
				<Item Name="Replace_nested_Action.vi" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/subvis/Replace_nested_Action.vi"/>
			</Item>
			<Item Name="Swish Configure.vi" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/Swish Configure.vi"/>
			<Item Name="Swish_Configure_UI.vi" Type="VI" URL="../../../../Program Files (x86)/National Instruments/LabVIEW 2014/project/_Swish/Swish_Configure_UI.vi"/>
		</Item>
		<Item Name="scratchpad" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="_log_exp_curves.vi" Type="VI" URL="../scratchpad/_log_exp_curves.vi"/>
			<Item Name="_test_class.lvclass" Type="LVClass" URL="../scratchpad/_test_class.lvclass"/>
			<Item Name="capture_right_click_event_append_swish_menu_items.vi" Type="VI" URL="../scratchpad/capture_right_click_event_append_swish_menu_items.vi"/>
			<Item Name="capture_right_click_guinea_pig_vi.vi" Type="VI" URL="../scratchpad/capture_right_click_guinea_pig_vi.vi"/>
			<Item Name="refresh_menus.vi" Type="VI" URL="../scratchpad/refresh_menus.vi"/>
			<Item Name="carousel_testing_1.vi" Type="VI" URL="../scratchpad/carousel_testing_1.vi"/>
			<Item Name="Dynamic_event_reregistration.vi" Type="VI" URL="../scratchpad/Dynamic_event_reregistration.vi"/>
			<Item Name="early_testing.vi" Type="VI" URL="../scratchpad/early_testing.vi"/>
			<Item Name="monitoring_mouse_test.vi" Type="VI" URL="../scratchpad/monitoring_mouse_test.vi"/>
			<Item Name="tag_testing.vi" Type="VI" URL="../scratchpad/tag_testing.vi"/>
			<Item Name="Stop_Swish_Service.vi" Type="VI" URL="../scratchpad/Stop_Swish_Service.vi"/>
			<Item Name="vi_and_control_tags_support_classes.vi" Type="VI" URL="../scratchpad/vi_and_control_tags_support_classes.vi"/>
			<Item Name="create_icons_as_2DPicture_constants.vi" Type="VI" URL="../scratchpad/create_icons_as_2DPicture_constants.vi"/>
			<Item Name="show all VIs in memory.vi" Type="VI" URL="../scratchpad/show all VIs in memory.vi"/>
		</Item>
		<Item Name="Swish_source" Type="Folder">
			<Item Name="LVGObject" Type="Folder">
				<Item Name="LVGControl" Type="Folder">
					<Item Name="LVGControl.lvclass" Type="LVClass" URL="../Swish_source/LVGObject/LVGControl/LVGControl.lvclass"/>
				</Item>
				<Item Name="LVGObject.lvclass" Type="LVClass" URL="../Swish_source/LVGObject/LVGObject.lvclass"/>
			</Item>
			<Item Name="SwishAction_class" Type="Folder">
				<Item Name="SwishDandD_class" Type="Folder">
					<Item Name="SwishDandD.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishDandD_class/SwishDandD.lvclass"/>
				</Item>
				<Item Name="SwishMotion_class" Type="Folder">
					<Item Name="Anchor" Type="Folder">
						<Item Name="Anchor.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishMotion_class/Anchor/Anchor.lvclass"/>
					</Item>
					<Item Name="common" Type="Folder">
						<Item Name="_unit_log.vi" Type="VI" URL="../Swish_source/SwishAction_class/SwishMotion_class/common/_unit_log.vi"/>
					</Item>
					<Item Name="MoveAdv" Type="Folder">
						<Item Name="MoveAdv.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishMotion_class/MoveAdv/MoveAdv.lvclass"/>
					</Item>
					<Item Name="MoveCarousel" Type="Folder">
						<Item Name="MoveCarousel.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishMotion_class/MoveCarousel/MoveCarousel.lvclass"/>
					</Item>
					<Item Name="MoveSimple" Type="Folder">
						<Item Name="MoveSimple.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishMotion_class/MoveSimple/MoveSimple.lvclass"/>
					</Item>
					<Item Name="MoveSwipe" Type="Folder">
						<Item Name="MoveSwipe.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishMotion_class/MoveSwipe/MoveSwipe.lvclass"/>
					</Item>
					<Item Name="SwishMotion.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishMotion_class/SwishMotion.lvclass"/>
				</Item>
				<Item Name="SwishResize_class" Type="Folder">
					<Item Name="SwishResize.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishResize_class/SwishResize.lvclass"/>
				</Item>
				<Item Name="SwishTap_class" Type="Folder">
					<Item Name="SwishTap.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishTap_class/SwishTap.lvclass"/>
				</Item>
				<Item Name="SwishVisibility_class" Type="Folder">
					<Item Name="SwishVisibility.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishVisibility_class/SwishVisibility.lvclass"/>
				</Item>
				<Item Name="typedefs" Type="Folder">
					<Item Name="action_execute_status.ctl" Type="VI" URL="../Swish_source/SwishAction_class/typedefs/action_execute_status.ctl"/>
					<Item Name="motion_mode_enum.ctl" Type="VI" URL="../Swish_source/SwishAction_class/typedefs/motion_mode_enum.ctl"/>
					<Item Name="move_axis_enum.ctl" Type="VI" URL="../Swish_source/SwishAction_class/typedefs/move_axis_enum.ctl"/>
				</Item>
				<Item Name="SwishAction.lvclass" Type="LVClass" URL="../Swish_source/SwishAction_class/SwishAction.lvclass"/>
			</Item>
			<Item Name="SwishCommon" Type="Folder">
				<Item Name="typedefs" Type="Folder">
					<Item Name="action info_cluster.ctl" Type="VI" URL="../Swish_source/SwishCommon/typedefs/action info_cluster.ctl"/>
					<Item Name="gobject_UID.ctl" Type="VI" URL="../Swish_source/SwishCommon/typedefs/gobject_UID.ctl"/>
				</Item>
				<Item Name="_Swish_replace_gobj_ref_from_ref_list.vi" Type="VI" URL="../Swish_source/SwishCommon/_Swish_replace_gobj_ref_from_ref_list.vi"/>
			</Item>
			<Item Name="SwishEvent_class" Type="Folder">
				<Item Name="SwishMouse_class" Type="Folder">
					<Item Name="MouseDown" Type="Folder">
						<Item Name="MouseDown.lvclass" Type="LVClass" URL="../Swish_source/SwishEvent_class/SwishMouse_class/MouseDown/MouseDown.lvclass"/>
					</Item>
					<Item Name="MouseEnter" Type="Folder">
						<Item Name="MouseEnter.lvclass" Type="LVClass" URL="../Swish_source/SwishEvent_class/SwishMouse_class/MouseEnter/MouseEnter.lvclass"/>
					</Item>
					<Item Name="MouseLeave" Type="Folder">
						<Item Name="MouseLeave.lvclass" Type="LVClass" URL="../Swish_source/SwishEvent_class/SwishMouse_class/MouseLeave/MouseLeave.lvclass"/>
					</Item>
					<Item Name="MouseUp" Type="Folder">
						<Item Name="MouseUp.lvclass" Type="LVClass" URL="../Swish_source/SwishEvent_class/SwishMouse_class/MouseUp/MouseUp.lvclass"/>
					</Item>
					<Item Name="SwishMouse.lvclass" Type="LVClass" URL="../Swish_source/SwishEvent_class/SwishMouse_class/SwishMouse.lvclass"/>
				</Item>
				<Item Name="SwishUserEvent_class" Type="Folder">
					<Item Name="SwishUserEvent.lvclass" Type="LVClass" URL="../Swish_source/SwishEvent_class/SwishUserEvent_class/SwishUserEvent.lvclass"/>
				</Item>
				<Item Name="typedefs" Type="Folder">
					<Item Name="event_responsibility_enum.ctl" Type="VI" URL="../Swish_source/SwishEvent_class/typedefs/event_responsibility_enum.ctl"/>
				</Item>
				<Item Name="SwishEvent.lvclass" Type="LVClass" URL="../Swish_source/SwishEvent_class/SwishEvent.lvclass"/>
			</Item>
			<Item Name="SwishMessaging_class" Type="Folder">
				<Item Name="client2reg_class" Type="Folder">
					<Item Name="dereg_object" Type="Folder">
						<Item Name="dereg_object.lvclass" Type="LVClass" URL="../Swish_source/SwishMessaging_class/client2reg_class/dereg_object/dereg_object.lvclass"/>
					</Item>
					<Item Name="reg_object" Type="Folder">
						<Item Name="reg_object.lvclass" Type="LVClass" URL="../Swish_source/SwishMessaging_class/client2reg_class/reg_object/reg_object.lvclass"/>
					</Item>
					<Item Name="client2reg.lvclass" Type="LVClass" URL="../Swish_source/SwishMessaging_class/client2reg_class/client2reg.lvclass"/>
				</Item>
				<Item Name="event2objcthndlr_class" Type="Folder">
					<Item Name="event2actnhndlr.lvclass" Type="LVClass" URL="../Swish_source/SwishMessaging_class/event2objcthndlr_class/event2actnhndlr.lvclass"/>
				</Item>
				<Item Name="reg2event_class" Type="Folder">
					<Item Name="new_object" Type="Folder">
						<Item Name="new_object.lvclass" Type="LVClass" URL="../Swish_source/SwishMessaging_class/reg2event_class/new_object/new_object.lvclass"/>
					</Item>
					<Item Name="removed_object" Type="Folder">
						<Item Name="removed_object.lvclass" Type="LVClass" URL="../Swish_source/SwishMessaging_class/reg2event_class/removed_object/removed_object.lvclass"/>
					</Item>
					<Item Name="reg2event.lvclass" Type="LVClass" URL="../Swish_source/SwishMessaging_class/reg2event_class/reg2event.lvclass"/>
				</Item>
				<Item Name="Swish_Message" Type="Folder">
					<Item Name="Swish_Message.lvclass" Type="LVClass" URL="../Swish_source/SwishMessaging_class/Swish_Message/Swish_Message.lvclass"/>
				</Item>
			</Item>
			<Item Name="SwishObject_class" Type="Folder">
				<Item Name="SwishControl_class" Type="Folder">
					<Item Name="methods" Type="Folder"/>
					<Item Name="private" Type="Folder"/>
					<Item Name="properties" Type="Folder"/>
					<Item Name="protected" Type="Folder"/>
					<Item Name="SwishControl.lvclass" Type="LVClass" URL="../Swish_source/SwishObject_class/SwishControl_class/SwishControl.lvclass"/>
				</Item>
				<Item Name="SwishFPanel_class" Type="Folder">
					<Item Name="SwishFPanel.lvclass" Type="LVClass" URL="../Swish_source/SwishObject_class/SwishFPanel_class/SwishFPanel.lvclass"/>
				</Item>
				<Item Name="SwishMultipleControls" Type="Folder">
					<Item Name="SwishMultipleControls.lvclass" Type="LVClass" URL="../Swish_source/SwishObject_class/SwishMultipleControls/SwishMultipleControls.lvclass"/>
				</Item>
				<Item Name="SwishSplitter_class" Type="Folder">
					<Item Name="SwishSplitter.lvclass" Type="LVClass" URL="../Swish_source/SwishObject_class/SwishSplitter_class/SwishSplitter.lvclass"/>
				</Item>
				<Item Name="SwishObject.lvclass" Type="LVClass" URL="../Swish_source/SwishObject_class/SwishObject.lvclass"/>
			</Item>
			<Item Name="SwishService" Type="Folder">
				<Item Name="_subvis" Type="Folder">
					<Item Name="_add_new_object_definition_cnstnt.vi" Type="VI" URL="../Swish_source/SwishService/_subvis/_add_new_object_definition_cnstnt.vi"/>
					<Item Name="all_stop_fg.vi" Type="VI" URL="../Swish_source/SwishService/_subvis/all_stop_fg.vi"/>
					<Item Name="Deregister_Swish_Objects.vi" Type="VI" URL="../Swish_source/SwishService/_subvis/Deregister_Swish_Objects.vi"/>
					<Item Name="gID_FG.vi" Type="VI" URL="../Swish_source/SwishService/_subvis/gID_FG.vi"/>
					<Item Name="gID_FG_task_enum.ctl" Type="VI" URL="../Swish_source/SwishService/_subvis/gID_FG_task_enum.ctl"/>
					<Item Name="Register_Swish_Objects.vi" Type="VI" URL="../Swish_source/SwishService/_subvis/Register_Swish_Objects.vi"/>
				</Item>
				<Item Name="ActionHandler" Type="Folder">
					<Item Name="_subvis" Type="Folder">
						<Item Name="actn_handlr_get_mouse_state.vi" Type="VI" URL="../Swish_source/SwishService/ActionHandler/_subvis/actn_handlr_get_mouse_state.vi"/>
						<Item Name="actn_handlr_global.vi" Type="VI" URL="../Swish_source/SwishService/ActionHandler/_subvis/actn_handlr_global.vi"/>
						<Item Name="create_actn_hndlr_q_ref.vi" Type="VI" URL="../Swish_source/SwishService/ActionHandler/_subvis/create_actn_hndlr_q_ref.vi"/>
						<Item Name="mouse_status_clstr.ctl" Type="VI" URL="../Swish_source/SwishService/ActionHandler/_subvis/mouse_status_clstr.ctl"/>
					</Item>
					<Item Name="swish_action_handler.vi" Type="VI" URL="../Swish_source/SwishService/ActionHandler/swish_action_handler.vi"/>
				</Item>
				<Item Name="EventHandler" Type="Folder">
					<Item Name="subvis" Type="Folder">
						<Item Name="create_event_ref.vi" Type="VI" URL="../Swish_source/SwishService/EventHandler/subvis/create_event_ref.vi"/>
						<Item Name="send_message_to_event_wrapper.vi" Type="VI" URL="../Swish_source/SwishService/EventHandler/subvis/send_message_to_event_wrapper.vi"/>
					</Item>
					<Item Name="typedefs" Type="Folder">
						<Item Name="dynamic_events_table.ctl" Type="VI" URL="../Swish_source/SwishService/EventHandler/typedefs/dynamic_events_table.ctl"/>
						<Item Name="event_registration_ref.ctl" Type="VI" URL="../Swish_source/SwishService/EventHandler/typedefs/event_registration_ref.ctl"/>
						<Item Name="event_userevent_clstr.ctl" Type="VI" URL="../Swish_source/SwishService/EventHandler/typedefs/event_userevent_clstr.ctl"/>
						<Item Name="object_table_element.ctl" Type="VI" URL="../Swish_source/SwishService/EventHandler/typedefs/object_table_element.ctl"/>
						<Item Name="Registered_events_cluster.ctl" Type="VI" URL="../Swish_source/SwishService/EventHandler/typedefs/Registered_events_cluster.ctl"/>
					</Item>
					<Item Name="swish_event_handler.vi" Type="VI" URL="../Swish_source/SwishService/EventHandler/swish_event_handler.vi"/>
				</Item>
				<Item Name="Registrants" Type="Folder">
					<Item Name="subvis" Type="Folder"/>
					<Item Name="swish_registrants.vi" Type="VI" URL="../Swish_source/SwishService/Registrants/swish_registrants.vi"/>
				</Item>
				<Item Name="registrants_FG" Type="Folder">
					<Item Name="subvis" Type="Folder">
						<Item Name="_remove_by_object.vi" Type="VI" URL="../Swish_source/SwishService/registrants_FG/subvis/_remove_by_object.vi"/>
					</Item>
					<Item Name="typedefs" Type="Folder">
						<Item Name="_active_swish_actions.ctl" Type="VI" URL="../Swish_source/SwishService/registrants_FG/typedefs/_active_swish_actions.ctl"/>
						<Item Name="registered_object_element.ctl" Type="VI" URL="../Swish_source/SwishService/registrants_FG/typedefs/registered_object_element.ctl"/>
					</Item>
					<Item Name="swish_regFG.vi" Type="VI" URL="../Swish_source/SwishService/registrants_FG/swish_regFG.vi"/>
					<Item Name="swish_regFG_append_wrapper.vi" Type="VI" URL="../Swish_source/SwishService/registrants_FG/swish_regFG_append_wrapper.vi"/>
					<Item Name="swish_regFG_remove_wrapper.vi" Type="VI" URL="../Swish_source/SwishService/registrants_FG/swish_regFG_remove_wrapper.vi"/>
					<Item Name="swish_regFG_task_enum.ctl" Type="VI" URL="../Swish_source/SwishService/registrants_FG/swish_regFG_task_enum.ctl"/>
				</Item>
				<Item Name="Tagging" Type="Folder">
					<Item Name="Swish_Tagging.lvlib" Type="Library" URL="../Swish_source/SwishService/Tagging/Swish_Tagging.lvlib"/>
				</Item>
				<Item Name="swish_service.vi" Type="VI" URL="../Swish_source/SwishService/swish_service.vi"/>
			</Item>
			<Item Name="SwishUE library" Type="Folder">
				<Item Name="SwishUE.lvlib" Type="Library" URL="../Swish_source/SwishUE library/SwishUE.lvlib"/>
			</Item>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="_Get Sound Error From Return Value.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_Get Sound Error From Return Value.vi"/>
				<Item Name="_GetConfiguration.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/_GetConfiguration.vi"/>
				<Item Name="Acquire Input Data.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Acquire Input Data.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check for Contained Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/VariantDataType/Check for Contained Data Type.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Input Device.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Close Input Device.vi"/>
				<Item Name="closeJoystick.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeJoystick.vi"/>
				<Item Name="closeKeyboard.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeKeyboard.vi"/>
				<Item Name="closeMouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeMouse.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrorDescriptions.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/ErrorDescriptions.vi"/>
				<Item Name="errorList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/errorList.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventsource.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventsource.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Initialize Mouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Initialize Mouse.vi"/>
				<Item Name="joystickAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/joystickAcquire.vi"/>
				<Item Name="keyboardAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/keyboardAcquire.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="lveventtype.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/lveventtype.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Menu Launch VI Information.vi" Type="VI" URL="/&lt;vilib&gt;/VIServer/Menu Launch VI Information.vi"/>
				<Item Name="mouseAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/mouseAcquire.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Query Input Devices.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Query Input Devices.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Sampling Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sampling Mode.ctl"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Sound Data Format.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Data Format.ctl"/>
				<Item Name="Sound Output Configure.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Configure.vi"/>
				<Item Name="Sound Output Set Volume (Array).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Set Volume (Array).vi"/>
				<Item Name="Sound Output Set Volume (Single).vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Set Volume (Single).vi"/>
				<Item Name="Sound Output Set Volume.vi" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Set Volume.vi"/>
				<Item Name="Sound Output Task ID.ctl" Type="VI" URL="/&lt;vilib&gt;/sound2/lvsound2.llb/Sound Output Task ID.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Type Specific Details.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/VariantDataType/Type Specific Details.ctl"/>
				<Item Name="UserTags.lvlib" Type="Library" URL="/&lt;vilib&gt;/UserTags/UserTags.lvlib"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="../../../../Windows/System32/kernel32.dll"/>
			<Item Name="LV_Audio.lvlib" Type="Library" URL="../../LabMu/code/LV_Audio/LV_Audio.lvlib"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="lvinput.dll" Type="Document" URL="/&lt;resource&gt;/lvinput.dll"/>
			<Item Name="lvsound2.dll" Type="Document" URL="/&lt;resource&gt;/lvsound2.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
